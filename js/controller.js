var app = angular.module("panhinda", []);

app.controller('application', function($scope){
  $scope.overlay1 = function(){
    $scope.items = [{panelHedding:"Link", panelContent:"linkContent"},
                    {panelHedding:"Note", panelContent:"noteContent"},
                    {panelHedding:"Note", panelContent:"noteContent"},
                    {panelHedding:"Note", panelContent:"noteContent"},
                    {panelHedding:"Graph", panelContent:"graphContent"}];
    $scope.selected = 1;
  };

  $scope.overlay2 = function(){
    $scope.items = [{panelHedding:"Note", panelContent:"noteContent"},
                    {panelHedding:"Note", panelContent:"noteContent"},
                    {panelHedding:"Note", panelContent:"noteContent"},];
    $scope.selected = 2;
  };

  $scope.overlay3 = function(){
    $scope.items = [{panelHedding:"Note", panelContent:"noteContent"},
                    {panelHedding:"MindMap", panelContent:"MindMapContent"},
                    {panelHedding:"Graph", panelContent:"graphContent"},
                    {panelHedding:"Note", panelContent:"noteContent"}];

    $scope.selected = 3;
  };

  $scope.overlay1();
});
